# Nestjs csv parser

For starting the application we need to configure **.env** just find **.env.sample** and rename to **.env**

- ./.env.sample
- ./app/.env.sample

Run the system with Docker Compose

```cmd
docker-compose up -d
```

You can find API by http://localhost:6868/

Stop the Application
Stopping all the running containers is also simple with a single command: **docker-compose down** If you need to stop and remove all containers, networks, and all images used by any service in docker-compose.yml file, use the command:

```cmd
docker-compose down --rmi all
```
