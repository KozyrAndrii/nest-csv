interface IUserDbModel extends IBaseDbModel {
	id: number;
	disabled: boolean;
	email: string;
	emailConfirmed: boolean;
	confirmEmailToken: string;
	forgotPasswordToken: null | string;
	name: string;
	password: string;
	salt: string;
	accessToken: string | null;
	role: TUserRole;
}
