interface ISourceDbModel extends IBaseDbModel {
	id: number;
	name: string;
}
