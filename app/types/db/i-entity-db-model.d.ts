interface IEntityDbModel extends IBaseDbModel {
	id: number;
	date: Date;
	description: string | null;
	number: number;
	sourceId: number;
}
