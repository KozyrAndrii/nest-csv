import { ExecutionContext, ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { includes } from 'lodash';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
	constructor(private reflector: Reflector) {
		super('jwt');
	}

	canActivate(context: ExecutionContext) {
		// Add your custom authentication logic here
		// for example, call super.logIn(request) to establish a session.
		return super.canActivate(context);
	}

	handleRequest(err, user, info, context) {
		// You can throw an exception based on either "info" or "err" arguments

		if (err || !user) {
			throw err || new UnauthorizedException();
		}

		const roles = this.reflector.get<string[]>('roles', context.getHandler());

		if (roles && !includes(roles, user.role)) {
			throw new ForbiddenException();
		}

		return user;
	}
}
