import { Transform, TransformOptions } from 'class-transformer';
import * as moment from 'moment';

export function StringToDate(
	format?: string,
	transformOptions?: TransformOptions,
): (target: any, key: string) => void {
	return Transform(({ value }: { value: unknown }) => {
		if ('string' !== typeof value || !value.trim()) {
			return value;
		}
		const momentDate = moment(value.trim(), format || 'DD-MM-YYYY');
		if (momentDate.isValid()) {
			return momentDate.toDate();
		}
		return value;
	}, transformOptions);
}
