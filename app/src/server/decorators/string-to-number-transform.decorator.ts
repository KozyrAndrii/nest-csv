import { Transform, TransformOptions } from 'class-transformer';

export function StringToNumber(transformOptions?: TransformOptions): (target: any, key: string) => void {
	return Transform(({ value }: { value: unknown }) => {
		if ('string' !== typeof value || !value.trim()) {
			return value;
		}
		const data: number = +value;

		return Number.isNaN(data) ? value : data;
	}, transformOptions);
}
