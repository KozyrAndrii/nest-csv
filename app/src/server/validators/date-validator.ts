import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'DateValidator' })
export class DateValidator implements ValidatorConstraintInterface {
	async validate(value: unknown) {
		return value instanceof Date;
	}

	defaultMessage(args: ValidationArguments) {
		return `Wrong date`;
	}
}
