import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const initializeSwagger = ({ app, version }) => {
	const config = new DocumentBuilder()
		.setTitle('test csv API')
		.setDescription('The csv API description')
		.setVersion(version)
		.addBearerAuth({
			type: 'http',
			scheme: 'bearer',
			in: 'header',
			bearerFormat: 'JWT',
		})
		.build();
	const document = SwaggerModule.createDocument(app, config);

	SwaggerModule.setup('/', app, document);
};
