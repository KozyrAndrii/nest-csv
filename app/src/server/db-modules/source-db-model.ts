import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EntityDbModel } from './entity-db-model';

@Entity('source')
export class SourceDbModel implements ISourceDbModel {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ length: 255 })
	name: string;

	@OneToMany((type) => EntityDbModel, (data) => data.source, { nullable: true, eager: true })
	@JoinColumn({ name: 'id', referencedColumnName: 'sourceId' })
	public data?: Array<EntityDbModel>;
}
