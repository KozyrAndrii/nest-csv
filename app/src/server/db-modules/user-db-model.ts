import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { UserRoleEnum } from '../enums/user-tole.enum';

@Entity('user')
export class UserDbModel implements Partial<IUserDbModel> {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ length: 255 })
	name: string;

	@Column({ length: 255 })
	email: string;

	@Column({ default: false })
	emailConfirmed: boolean;

	@Column({ default: false })
	disabled: boolean;

	@Column({ length: 255 })
	password: string;

	@Column({ length: 32 })
	salt: string;

	@Column({ length: 512 })
	accessToken: string;

	@Column({
		type: 'enum',
		enum: UserRoleEnum,
		default: UserRoleEnum.GHOST,
	})
	role: UserRoleEnum;
}
