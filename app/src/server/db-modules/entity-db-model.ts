import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SourceDbModel } from './source-db-model';

@Entity('entity')
export class EntityDbModel implements IEntityDbModel {
	@PrimaryGeneratedColumn()
	public id: number;

	@Column({ type: 'timestamp' })
	public date: Date;

	@Column({ type: 'text' })
	public description: string | null;

	@Column({
		type: 'int',
		default: '0',
	})
	public number: number;

	@Column({ type: 'int' })
	public sourceId: number;

	@ManyToOne((type) => SourceDbModel, (source) => source.data)
	@JoinColumn({ name: 'sourceId' })
	public source?: SourceDbModel;
}
