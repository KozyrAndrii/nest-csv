import { config } from 'dotenv';

config();

export const SERVER_CONFIG = {
	PORT: process.env.PORT || 3000,
	JWT_CONFIG: {
		secret: process.env.JWT_SECRET || 'secretKey',
		expiresIn: '5h',
		confirmEmailExpiresIn: '2 days',
	},
	DB_CONFIG: {
		host: process.env.DB_HOST || '127.0.0.1',
		user: process.env.DB_USER || 'root',
		password: process.env.DB_PASSWORD || '123456',
		database: process.env.DB_NAME || 'csvtest',
		port: Number(process.env.DB_PORT || 3306),
	},
};
