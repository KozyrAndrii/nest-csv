import * as CryptoJS from 'crypto-js';

class CipherText {
	constructor() {
		this.getSecret = this.getSecret.bind(this);
		this.encrypt = this.encrypt.bind(this);
		this.decrypt = this.decrypt.bind(this);
		this.md5 = this.md5.bind(this);
	}

	public md5(str: string): string {
		return CryptoJS.MD5(str).toString();
	}

	public getSecret(number = 32): string {
		let result = '';
		const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		const charactersLength = characters.length;

		for (let i = 0; i < number; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}

		return result;
	}

	public encrypt(text: string, secretWord = ''): ICipherTextEncrypt {
		const secret = secretWord ? secretWord : this.getSecret();

		return {
			secret,
			cipher: CryptoJS.AES.encrypt(text, secret).toString(),
		};
	}

	public decrypt(cipher, secret): string {
		const bytes = CryptoJS.AES.decrypt(cipher, secret);

		return bytes.toString(CryptoJS.enc.Utf8);
	}
}

export const cipherText = new CipherText();
