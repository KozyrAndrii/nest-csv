import * as jwt from 'jsonwebtoken';
import { SERVER_CONFIG } from '../constants/server.config';

const { JWT_CONFIG } = SERVER_CONFIG;

export const getJwtToken = (id: number, deadline = JWT_CONFIG.expiresIn): string => {
	return jwt.sign({ id }, JWT_CONFIG.secret, { expiresIn: deadline });
};

export const decodedJwtToken = async (token: string) => {
	return jwt.verify(token, JWT_CONFIG.secret);
};
