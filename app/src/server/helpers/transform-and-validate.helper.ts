import { ClassConstructor, plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { GeneralValidationError } from '../errors/general-validation.error';

export const transformAndValidate = async <T>({
	data,
	Model,
}: {
	data: unknown;
	Model: ClassConstructor<T>;
}): Promise<T> => {
	const model: T = plainToClass(Model, data || {}, { excludeExtraneousValues: true });
	const validationErrors = await validate(model as object);

	if (validationErrors.length) {
		throw new GeneralValidationError(validationErrors);
	}

	return model;
};
