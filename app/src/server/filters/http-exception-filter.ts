import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { get } from 'lodash';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
	catch(exception: HttpException, host: ArgumentsHost) {
		const type = get(exception, 'constructor.name');
		const validations = get(exception, 'validations', null);
		const ctx = host.switchToHttp();
		const response = ctx.getResponse<Response>();
		const request = ctx.getRequest<Request>();
		const status = exception.getStatus();
		const result = exception.getResponse();
		const message = typeof result === 'string' ? result : get(result, 'error');

		console.log(['http_exception'], { type, path: request.url, message });

		response.status(status).json({
			statusCode: status,
			message,
			type,
			validations,
		});
	}
}
