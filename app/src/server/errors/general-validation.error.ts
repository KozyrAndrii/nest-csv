import { HttpException, HttpStatus } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { forEach, size } from 'lodash';

export class GeneralValidationError extends HttpException {
	constructor(validationErrors: ValidationError[]) {
		let firstMessage = '';
		const validations = [];

		forEach(validationErrors, (validationError) => {
			if (size(validationError.constraints)) {
				forEach(validationError.constraints, (message, rule) => {
					if (message) {
						validations.push({
							field: validationError.property,
							message,
							rule,
						});
						if (!firstMessage) {
							firstMessage = message;
						}
					}
				});
			}
		});

		super(firstMessage, HttpStatus.BAD_REQUEST);

		Object.assign(this, { validations });
	}
}
