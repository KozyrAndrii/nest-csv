import { HttpException, HttpStatus } from '@nestjs/common';

export class AlreadyExistsError extends HttpException {
	constructor(name: string, value: string | number | boolean) {
		super(`The ${name} with value ${value} already exists`, HttpStatus.BAD_REQUEST);
	}
}
