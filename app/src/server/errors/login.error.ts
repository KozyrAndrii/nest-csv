import { HttpException, HttpStatus } from '@nestjs/common';

export class LoginError extends HttpException {
	constructor() {
		super('Email or password is incorrect', HttpStatus.BAD_REQUEST);
	}
}
