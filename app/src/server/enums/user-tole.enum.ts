export enum UserRoleEnum {
	GHOST = 'GHOST',
	ADMIN = 'ADMIN',
	USER = 'USER',
}
