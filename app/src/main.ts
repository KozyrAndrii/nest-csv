import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { json, urlencoded } from 'body-parser';
import { ValidationError } from 'class-validator';
import { AppModule } from './app.module';
import { SERVER_CONFIG } from './server/constants/server.config';
import { GeneralValidationError } from './server/errors/general-validation.error';
import { initializeSwagger } from './server/initialize-swagger';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.use(json({ limit: '50mb' }));
	app.use(urlencoded({ limit: '50mb', extended: true }));

	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
			disableErrorMessages: true,
			exceptionFactory: (validationErrors: ValidationError[] = []) => {
				if (validationErrors.length) {
					return new GeneralValidationError(validationErrors);
				}
				return new BadRequestException(validationErrors);
			},
		}),
	);

	initializeSwagger({
		app,
		version: 1,
	});
	await app.listen(SERVER_CONFIG.PORT);
}
bootstrap();
