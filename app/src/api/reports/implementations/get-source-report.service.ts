import { Injectable } from '@nestjs/common';
import { forEach, groupBy } from 'lodash';
import { GetSourceReportRepositoryService } from './get-source-report-repository.service';

@Injectable()
export class GetSourceReportService {
	constructor(private readonly _importRepositoryService: GetSourceReportRepositoryService) {}

	public async handleRequest(model: IReportFilter) {
		const results = await this._importRepositoryService.getItems(model);

		const data = [];
		forEach(groupBy(results, 'source_name'), (items, source) => {
			data.push({
				source,
				data: items.map(({ monthYear, data_sum }) => ({
					date: monthYear,
					sum: data_sum,
				})),
			});
		});
		return data;
	}
}
