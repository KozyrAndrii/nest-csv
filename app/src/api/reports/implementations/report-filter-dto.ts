import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, Length, Matches } from 'class-validator';
import { Trim } from '../../../server/decorators/trim-transform.decorator';

export class ReportFilterDto implements IReportFilter {
	@ApiProperty({
		example: '02-2022',
		required: false,
	})
	@Expose()
	@IsOptional()
	@Matches(/^(1[0-2]|0[1-9])\-(20\d{2}|19\d{2})$/, {
		message: 'Should contain month year only. For example 02-2022',
	})
	public date?: string;

	@ApiProperty({
		example: 'other',
		required: false,
	})
	@Expose()
	@IsOptional()
	@Trim()
	@IsString()
	@IsNotEmpty()
	@Length(1, 255)
	public source?: string;
}
