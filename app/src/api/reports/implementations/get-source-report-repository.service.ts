import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EntityDbModel } from '../../../server/db-modules/entity-db-model';
import { SourceDbModel } from '../../../server/db-modules/source-db-model';

@Injectable()
export class GetSourceReportRepositoryService {
	constructor(
		@InjectRepository(SourceDbModel)
		private _sourcesRepository: Repository<SourceDbModel>,
		@InjectRepository(EntityDbModel)
		private _entitiesRepository: Repository<EntityDbModel>,
	) {}

	public async getItems({ source, date }: IReportFilter): Promise<ISourceDbModel[]> {
		let query = this._sourcesRepository
			.createQueryBuilder('source')
			.select([
				'source.name',
				'data.date',
				"DATE_FORMAT(data.date, '%m-%Y') AS monthYear",
				'SUM(data.number) as data_sum',
			])
			.innerJoin('source.data', 'data')
			.groupBy('source.id')
			.addGroupBy('monthYear');
		if (source) {
			query = query.where({ name: source });
		}
		if (date) {
			query = query.andWhere(`DATE_FORMAT(data.date, '%m-%Y') = '${date}'`);
		}

		const results = await query.getRawMany();
		return results;
	}
}
