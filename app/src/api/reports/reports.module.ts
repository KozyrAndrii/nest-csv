import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityDbModel } from 'src/server/db-modules/entity-db-model';
import { SourceDbModel } from 'src/server/db-modules/source-db-model';
import { GetSourceReportRepositoryService } from './implementations/get-source-report-repository.service';
import { GetSourceReportService } from './implementations/get-source-report.service';
import { ReportsController } from './reports.controller';

@Module({
	imports: [TypeOrmModule.forFeature([SourceDbModel, EntityDbModel])],
	controllers: [ReportsController],
	providers: [GetSourceReportService, GetSourceReportRepositoryService],
	exports: [],
})
export class ReportsModule {}
