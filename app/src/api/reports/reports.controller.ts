import { Controller, Get, Query, UseFilters, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/server/authorization/jwt-auth.guard';
import { Roles } from 'src/server/decorators/roles.decorator';
import { UserRoleEnum } from 'src/server/enums/user-tole.enum';
import { HttpExceptionFilter } from 'src/server/filters/http-exception-filter';
import { GetSourceReportService } from './implementations/get-source-report.service';
import { ReportFilterDto } from './implementations/report-filter-dto';

@ApiTags('api | reports')
@Controller('api/reports')
export class ReportsController {
	constructor(private readonly _getSourceReportService: GetSourceReportService) {}

	@ApiBearerAuth()
	@Get('/')
	@Roles(UserRoleEnum.ADMIN)
	@UseGuards(JwtAuthGuard)
	@UseFilters(HttpExceptionFilter)
	async getSourceReport(@Query() model: ReportFilterDto) {
		return await this._getSourceReportService.handleRequest(model);
	}
}
