import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from 'src/modules/user/user.module';
import { SERVER_CONFIG } from 'src/server/constants/server.config';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './implementations/authentication.service';

import { JwtStrategy } from './implementations/jwt.strategy';

const { JWT_CONFIG } = SERVER_CONFIG;
@Module({
	imports: [
		UserModule,
		PassportModule,
		JwtModule.register({
			secret: JWT_CONFIG.secret,
			signOptions: { expiresIn: JWT_CONFIG.expiresIn },
		}),
	],
	controllers: [AuthenticationController],
	providers: [AuthenticationService, JwtStrategy],
	exports: [AuthenticationService, JwtModule],
})
export class AuthenticationModule {}
