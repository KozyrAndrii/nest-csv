import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsEmail, IsNotEmpty, IsString, Length, MaxLength, MinLength } from 'class-validator';

export class UserDto implements Partial<IUserDbModel> {
	@ApiProperty()
	@Expose()
	@IsString()
	@IsEmail()
	email!: string;

	@ApiProperty()
	@Expose()
	@MinLength(3)
	@MaxLength(255)
	@IsString()
	name!: string;

	@ApiProperty()
	@Expose()
	@IsString()
	@IsNotEmpty()
	@Length(6, 32)
	password!: string;
}
