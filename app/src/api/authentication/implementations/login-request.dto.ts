import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class LoginRequestDto {
	@ApiProperty({ example: 'mail@example.com' })
	@IsString()
	@IsEmail()
	email: string;

	@ApiProperty({ example: 'password1' })
	@IsString()
	@IsNotEmpty()
	@Length(6, 32)
	password: string;
}
