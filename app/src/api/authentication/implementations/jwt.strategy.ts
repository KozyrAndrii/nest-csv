import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from 'src/modules/user/implementations/user.service';

import { SERVER_CONFIG } from 'src/server/constants/server.config';

const { JWT_CONFIG } = SERVER_CONFIG;

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private readonly _usersService: UserService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: JWT_CONFIG.secret,
		});
	}

	async validate(payload: any) {
		const user = await this._usersService.getUserById(payload.id);

		if (!user) {
			throw new UnauthorizedException();
		}
		return user;
	}
}
