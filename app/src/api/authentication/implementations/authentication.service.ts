import { Injectable } from '@nestjs/common';
import { UserService } from 'src/modules/user/implementations/user.service';
import { SERVER_CONFIG } from 'src/server/constants/server.config';
import { UserRoleEnum } from 'src/server/enums/user-tole.enum';
import { AlreadyExistsError } from 'src/server/errors/already-exists.error';
import { LoginError } from 'src/server/errors/login.error';
import { cipherText } from 'src/server/helpers/cipher-text';
import { getJwtToken } from 'src/server/helpers/jwt-token';

const { JWT_CONFIG } = SERVER_CONFIG;

@Injectable()
export class AuthenticationService {
	constructor(private readonly _userService: UserService) {}

	public async register(model: Partial<IUserDbModel>) {
		const user: Partial<IUserDbModel> | undefined = await this._userService.getUserByEmail(model.email || '');

		if (user) {
			throw new AlreadyExistsError('email', String(model.email));
		}

		const { cipher: newPassword, secret } = cipherText.encrypt(cipherText.md5(model.password));

		model.password = newPassword;
		model.salt = secret;
		model.role = UserRoleEnum.ADMIN;

		const newUser: Partial<IUserDbModel> = await this._userService.createUser(model);
		const accessToken = await this._getAccessToken(newUser);
		await this._userService.setUserAccessToken(newUser.id, accessToken);

		return this._userService.getVieUserById(newUser.id);
	}

	public async login(email: string, password: string) {
		const user: Partial<IUserDbModel> | undefined = await this._userService.getUserByEmail(email);

		if (!user) {
			throw new LoginError();
		}

		const userPassword = cipherText.decrypt(user.password, user.salt);

		if (userPassword !== cipherText.md5(password)) {
			throw new LoginError();
		}

		const accessToken = await this._getAccessToken(user);

		await this._userService.setUserAccessToken(user.id, accessToken);

		return this._userService.getVieUserById(user.id);
	}

	private async _getAccessToken(user: Partial<IUserDbModel>): Promise<string> {
		const deadline = JWT_CONFIG.expiresIn;

		return getJwtToken(user.id, deadline);
	}
}
