import { Body, Controller, Post, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { HttpExceptionFilter } from 'src/server/filters/http-exception-filter';

import { AuthenticationService } from './implementations/authentication.service';
import { LoginRequestDto } from './implementations/login-request.dto';
import { UserDto } from './implementations/user-dto';

@ApiTags('api | authentication')
@Controller('api/auth')
export class AuthenticationController {
	constructor(private readonly _authService: AuthenticationService) {}

	@Post('/')
	@UseFilters(HttpExceptionFilter)
	async login(@Body() { email, password }: LoginRequestDto) {
		return await this._authService.login(email, password);
	}

	@Post('/register')
	@UseFilters(HttpExceptionFilter)
	async register(@Body() model: UserDto) {
		return await this._authService.register(model);
	}
}
