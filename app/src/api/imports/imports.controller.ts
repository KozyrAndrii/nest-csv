import { Body, Controller, Post, UploadedFile, UseFilters, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { parse } from 'papaparse';
import { JwtAuthGuard } from 'src/server/authorization/jwt-auth.guard';
import { ApiFile } from 'src/server/decorators/api-file.decorator';
import { Roles } from 'src/server/decorators/roles.decorator';
import { UserRoleEnum } from 'src/server/enums/user-tole.enum';
import { HttpExceptionFilter } from 'src/server/filters/http-exception-filter';
import { transformAndValidate } from 'src/server/helpers/transform-and-validate.helper';
import { CsvRequestDto } from './implementations/csv-request-dto';
import { ImportService } from './implementations/import.service';

@ApiTags('api | import')
@Controller('api/imports')
export class ImportsController {
	constructor(private readonly _importService: ImportService) {}

	@ApiBearerAuth()
	@Post('/')
	@Roles(UserRoleEnum.ADMIN)
	@UseGuards(JwtAuthGuard)
	@UseFilters(HttpExceptionFilter)
	async importJson(@Body() { items }: CsvRequestDto) {
		return await this._importService.handleRequest(items);
	}

	@ApiBearerAuth()
	@ApiConsumes('multipart/form-data')
	@ApiFile()
	@Post('upload')
	@UseInterceptors(FileInterceptor('file'))
	async uploadFile(@UploadedFile('file') file: Express.Multer.File) {
		const parsResult = parse(file.buffer.toString(), {
			header: true,
		});

		const rawData = { items: parsResult.data };

		const data = await transformAndValidate({
			Model: CsvRequestDto,
			data: rawData,
		});

		return await this._importService.handleRequest(data.items);
	}
}
