import { Expose } from 'class-transformer';
import {
	IsInt,
	IsNotEmpty,
	IsOptional,
	IsPositive,
	IsString,
	Length,
	MaxLength,
	Validate,
} from 'class-validator';
import { StringToDate } from 'src/server/decorators/string-to-date-transform.decorator';
import { StringToNumber } from 'src/server/decorators/string-to-number-transform.decorator';
import { Trim } from 'src/server/decorators/trim-transform.decorator';
import { DateValidator } from 'src/server/validators/date-validator';

export class CsvItemDto implements ICsvItem {
	@Expose()
	@StringToDate()
	@Validate(DateValidator)
	public date!: Date;

	@Expose({ name: 'sum' })
	@StringToNumber()
	@IsInt()
	@IsPositive()
	public number: number;

	@Expose()
	@Trim()
	@IsString()
	@IsNotEmpty()
	@Length(1, 255)
	public source!: string;

	@Expose()
	@Trim()
	@IsString()
	@IsOptional()
	@MaxLength(1024)
	public description!: string;
}
