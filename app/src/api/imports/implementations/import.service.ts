import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

import { ImportRepositoryService } from './import-repository.service';

@Injectable()
export class ImportService {
	constructor(private readonly _importRepositoryService: ImportRepositoryService) {}

	public async handleRequest(items: ICsvItem[]) {
		await this._saveSources(items);
		await this._saveEntity(items);

		return items;
	}

	private async _saveEntity(items: ICsvItem[]) {
		const models: Partial<IEntityDbModel>[] = await this._buildEntities(items);
		const newItems: Partial<IEntityDbModel>[] = [];
		const toUpdateItems: IEntityDbModel[] = [];
		const entityDictionary: Record<string, Date> = {};

		models.forEach(({ date }: IEntityDbModel) => {
			entityDictionary[moment(date).format('YYYY-MM-DD')] = date;
		});

		const existedEntities: IEntityDbModel[] = await this._importRepositoryService.getEntitiesByDays(
			Object.values(entityDictionary),
		);
		const existedEntityDictionary: Record<string, IEntityDbModel> = {};
		existedEntities.forEach(({ id, date, description, number, sourceId }: IEntityDbModel) => {
			const key = `${date.toISOString()}|${sourceId}`;
			existedEntityDictionary[key] = { id, date, description, number, sourceId };
		});

		models.forEach(({ date, description, number, sourceId }: Partial<IEntityDbModel>) => {
			const key = `${date.toISOString()}|${sourceId}`;
			const entity = existedEntityDictionary[key];

			if (!entity) {
				newItems.push({ date, description, number, sourceId });
			} else if (entity.description !== description || entity.number !== number) {
				toUpdateItems.push({ id: entity.id, date, description, number, sourceId });
			}
		});

		if (newItems.length) {
			await this._importRepositoryService.saveNewEntities(newItems);
		}

		if (toUpdateItems.length) {
			await Promise.all(
				toUpdateItems.map(({ id, ...model }: IEntityDbModel) => {
					return this._importRepositoryService.updateEntity(id, model);
				}),
			);
		}
	}

	private async _buildEntities(items: ICsvItem[]): Promise<Partial<IEntityDbModel>[]> {
		const sourceDictionary: Record<string, number> = await this._getSourceDictionary();
		const models: Partial<IEntityDbModel>[] = [];
		const entityFilter: Record<string, true> = {};

		items.forEach(({ date, number, source, description }: ICsvItem) => {
			const sourceId = sourceDictionary[source.toLowerCase()];
			if (!sourceId) {
				return;
			}

			const model: Partial<IEntityDbModel> = {
				date,
				number,
				description: description || null,
				sourceId,
			};
			const key = `${sourceId}|${date}`;

			if (entityFilter[key]) {
				return;
			}
			entityFilter[key] = true;
			models.push(model);
		});

		return models;
	}

	private async _getSourceDictionary(): Promise<Record<string, number>> {
		const sources: ISourceDbModel[] = await this._importRepositoryService.getAllSources();
		const sourceDictionary: Record<string, number> = {};
		sources.forEach(({ id, name }: ISourceDbModel) => {
			sourceDictionary[name.toLowerCase()] = id;
		});
		return sourceDictionary;
	}

	private async _saveSources(items: ICsvItem[]) {
		const sources: Record<string, string> = {};
		items.forEach(({ source }: ICsvItem) => {
			sources[source.toLowerCase()] = source;
		});

		const names: ISourceDbModel[] = await this._importRepositoryService.getBySourcesByNames(
			Object.values(sources),
		);

		names.forEach(({ name }: ISourceDbModel) => {
			delete sources[name.toLowerCase()];
		});

		const newItems: Partial<ISourceDbModel>[] = Object.values(sources).map((name: string) => ({ name }));

		if (newItems.length) {
			await this._importRepositoryService.saveSources(newItems);
		}
	}
}
