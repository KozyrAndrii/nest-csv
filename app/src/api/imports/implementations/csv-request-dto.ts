import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CsvItemDto } from './csv-item-dto';

export class CsvRequestDto {
	@ApiProperty({
		type: () => CsvItemDto,
		example: [
			{
				date: '15-02-2022',
				sum: 600,
				source: 'other',
				description: 'transactions',
			},
			{
				date: '15-02-2022',
				sum: 400,
				source: 'typical',
			},
		],
	})
	@Expose()
	@IsArray()
	@ValidateNested({ each: true })
	@Type(() => CsvItemDto)
	public items!: CsvItemDto[];
}
