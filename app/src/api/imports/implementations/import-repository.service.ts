import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityDbModel } from 'src/server/db-modules/entity-db-model';
import { Repository } from 'typeorm';
import { SourceDbModel } from '../../../server/db-modules/source-db-model';

@Injectable()
export class ImportRepositoryService {
	constructor(
		@InjectRepository(SourceDbModel)
		private _sourcesRepository: Repository<SourceDbModel>,
		@InjectRepository(EntityDbModel)
		private _entitiesRepository: Repository<EntityDbModel>,
	) {}

	public async getBySourcesByNames(names: string[]): Promise<ISourceDbModel[]> {
		return this._sourcesRepository.createQueryBuilder().where('name IN(:...names)', { names }).getMany();
	}

	public async saveSources(items: Partial<ISourceDbModel>[]): Promise<void> {
		await this._sourcesRepository.createQueryBuilder().insert().into(SourceDbModel).values(items).execute();
	}

	public async getAllSources(): Promise<ISourceDbModel[]> {
		return this._sourcesRepository.find();
	}

	public async getEntitiesByDays(days: Date[]): Promise<IEntityDbModel[]> {
		return this._entitiesRepository.createQueryBuilder().where('date IN(:...days)', { days }).getMany();
	}

	public async saveNewEntities(items: Partial<IEntityDbModel>[]): Promise<void> {
		await this._entitiesRepository.createQueryBuilder().insert().into(EntityDbModel).values(items).execute();
	}

	public async updateEntity(id: number, model: Partial<IEntityDbModel>) {
		return this._entitiesRepository.update({ id }, model);
	}
}
