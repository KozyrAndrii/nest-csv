import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityDbModel } from 'src/server/db-modules/entity-db-model';
import { SourceDbModel } from 'src/server/db-modules/source-db-model';
import { ImportRepositoryService } from './implementations/import-repository.service';
import { ImportService } from './implementations/import.service';
import { ImportsController } from './imports.controller';

@Module({
	imports: [TypeOrmModule.forFeature([SourceDbModel, EntityDbModel])],
	controllers: [ImportsController],
	providers: [ImportService, ImportRepositoryService],
	exports: [],
})
export class ImportsModule {}
