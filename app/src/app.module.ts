import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenticationModule } from './api/authentication/authentication.module';
import { ImportsModule } from './api/imports/imports.module';
import { ReportsModule } from './api/reports/reports.module';
import { SERVER_CONFIG } from './server/constants/server.config';
import { EntityDbModel } from './server/db-modules/entity-db-model';
import { SourceDbModel } from './server/db-modules/source-db-model';
import { UserDbModel } from './server/db-modules/user-db-model';

const { DB_CONFIG } = SERVER_CONFIG;

@Module({
	imports: [
		TypeOrmModule.forRoot({
			type: 'mysql',
			host: DB_CONFIG.host,
			port: DB_CONFIG.port,
			username: DB_CONFIG.user,
			password: DB_CONFIG.password,
			database: DB_CONFIG.database,
			entities: [UserDbModel, SourceDbModel, EntityDbModel],
			synchronize: false,
			logging: false,
		}),

		AuthenticationModule,
		ImportsModule,
		ReportsModule,
	],
	controllers: [],
	providers: [],
})
export class AppModule {}
