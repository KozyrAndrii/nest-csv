import { Injectable } from '@nestjs/common';
import { UserRepositoryService } from './user-repository.service';

@Injectable()
export class UserService {
	constructor(private readonly _userRepositoryService: UserRepositoryService) {}

	public async getVieUserById(id: number): Promise<Partial<IUserDbModel> | undefined> {
		return this._userRepositoryService.getVieUserById(id);
	}

	public async getUserById(id: number): Promise<Partial<IUserDbModel> | undefined> {
		return this._userRepositoryService.getVieUserById(id);
	}

	public async getUserByEmail(email: string): Promise<Partial<IUserDbModel> | undefined> {
		return this._userRepositoryService.getUserByEmail(email);
	}

	public async setUserAccessToken(id: number, accessToken: string): Promise<void> {
		await this._userRepositoryService.setUserAccessToken(id, accessToken);
	}

	public async createUser(model: Partial<IUserDbModel>) {
		return this._userRepositoryService.createUser(model);
	}
}
