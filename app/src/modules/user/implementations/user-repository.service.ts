import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDbModel } from '../../../server/db-modules/user-db-model';

@Injectable()
export class UserRepositoryService {
	constructor(
		@InjectRepository(UserDbModel)
		private usersRepository: Repository<UserDbModel>,
	) {}

	public async getVieUserById(id: number): Promise<Partial<IUserDbModel>> {
		return this.usersRepository.findOne({
			where: { id },
			select: { name: true, email: true, accessToken: true, role: true },
		});
	}

	public async getUserById(id: number): Promise<Partial<IUserDbModel>> {
		return this.usersRepository.findOneBy({ id });
	}

	public async getUserByEmail(email: string): Promise<Partial<IUserDbModel>> {
		return this.usersRepository.findOneBy({ email });
	}

	public async setUserAccessToken(id: number, accessToken: string) {
		return this.usersRepository.update({ id }, { accessToken });
	}

	public async createUser(model: Partial<IUserDbModel>): Promise<Partial<IUserDbModel>> {
		const user = new UserDbModel();
		Object.assign(user, model);
		return this.usersRepository.save(user);
	}
}
