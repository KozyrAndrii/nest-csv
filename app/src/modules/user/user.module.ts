import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserDbModel } from 'src/server/db-modules/user-db-model';
import { UserRepositoryService } from './implementations/user-repository.service';
import { UserService } from './implementations/user.service';

@Module({
	controllers: [],
	imports: [TypeOrmModule.forFeature([UserDbModel])],
	providers: [UserRepositoryService, UserService],
	exports: [UserService],
})
export class UserModule {}
