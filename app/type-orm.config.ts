import { DataSource } from 'typeorm';
import { InitUserTable1668530327860 } from './migrations/1668530327860-init-user-table';
import { InitSourceTable1668587632004 } from './migrations/1668587632004-init-source-table';
import { InitEntityTable1668588201293 } from './migrations/1668588201293-init-entity-table';
import { SERVER_CONFIG } from './src/server/constants/server.config';

const { DB_CONFIG } = SERVER_CONFIG;

export default new DataSource({
	type: 'mariadb',
	host: DB_CONFIG.host,
	port: DB_CONFIG.port,
	username: DB_CONFIG.user,
	password: DB_CONFIG.password,
	database: DB_CONFIG.database,
	entities: [],
	migrations: [InitUserTable1668530327860, InitSourceTable1668587632004, InitEntityTable1668588201293],
});
