import { MigrationInterface, QueryRunner } from 'typeorm';

const tableName = 'entity';

export class InitEntityTable1668588201293 implements MigrationInterface {
	async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE TABLE \`entity\` (
            \`id\` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            \`date\` DATE NOT NULL,
            \`number\` INT(10) NOT NULL DEFAULT '0',
            \`sourceId\` INT(10) UNSIGNED NOT NULL DEFAULT '0',
            \`description\` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
            PRIMARY KEY (\`id\`) USING BTREE,
            UNIQUE INDEX \`entity_sourceId_date_unique\` (\`sourceId\`, \`date\`) USING BTREE,
            INDEX \`entity_sourceId_foreign\` (\`sourceId\`) USING BTREE,
            CONSTRAINT \`entity_sourceId_foreign\` FOREIGN KEY (\`sourceId\`) REFERENCES \`source\` (\`id\`) ON UPDATE RESTRICT ON DELETE CASCADE
        )
        COLLATE='utf8mb4_general_ci'
        ENGINE=InnoDB
        ;`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable(tableName);
	}
}
