import { MigrationInterface, QueryRunner, Table } from 'typeorm';

enum UserRole {
	GHOST = 'GHOST',
	ADMIN = 'ADMIN',
	USER = 'USER',
}

const tableName = 'user';

export class InitUserTable1668530327860 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: tableName,
				columns: [
					{
						name: 'id',
						type: 'int',
						isPrimary: true,
						isGenerated: true,
						unsigned: true,
						generationStrategy: 'increment',
					},
					{
						name: 'name',
						type: 'varchar',
						length: '255',
					},
					{
						name: 'email',
						type: 'varchar',
						length: '255',
					},

					{
						name: 'emailConfirmed',
						type: 'boolean',
						default: false,
					},
					{
						name: 'disabled',
						type: 'boolean',
						default: false,
					},
					{
						name: 'password',
						type: 'varchar',
						length: '255',
					},
					{
						name: 'salt',
						type: 'varchar',
						length: '32',
					},
					{
						name: 'accessToken',
						type: 'varchar',
						length: '512',
						isNullable: true,
					},
					{
						name: 'role',
						type: 'ENUM',
						enum: [UserRole.GHOST, UserRole.ADMIN, UserRole.USER],
						default: `'${UserRole.GHOST}' COLLATE 'utf8mb4_general_ci'`,
					},
				],
			}),
			true,
		);

		await queryRunner.query(`INSERT INTO user (\`name\`, \`email\`, \`password\`, \`salt\`, \`emailConfirmed\`, \`role\`)
		 VALUES ('Admin', 'mail@example.com', 'U2FsdGVkX1/paQHPVTjIs2XAmS5gmWDKeeP1a0NGEE1zHWpdZCcb7XBHXTGCPxF+OhBvCSGf1FiF9KWj+8Babg==', 'salt', true, '${UserRole.ADMIN}');
		 `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable(tableName);
	}
}
