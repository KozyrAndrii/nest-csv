Stack:

Framework: NestJS

- DB: any SQL db
- ORM: Mikro ORM or TypeORM
- As an input you are given a CSV file with financial transactions.

Your goal is to upload the file with transactions using http,

- parse and store them in a database.
- CSV file has the following structure:
  - `date` | `sum` | `source` | `description`

The application must have a report route that will return grouped transactions for the user.
The report should return the data in the following format:

```json
    [{source: ‘name’, data: [{date: ‘month+year’, total:‘’ }]}]
```

where the total is the sum of sums for a selected month and source.
By default it should return grouped data for all dates and sources.

## Optional:

### Registration and authentication.

Users should have the possibility to register and authenticate in-app. Simple email/password pair and JWT auth.

We need data returned next way

EXAMPLE!

```json
[
  {
    "source": "Income",
    "data": [
      {
        "date": "01-2022",
        "total": 200
      },
      {
        "date": "02-2022",
        "total": 100
      }
    ]
  },
  {
    "source": "Other",
    "data": [
      {
        "date": "01-2022",
        "total": 1
      },
      {
        "date": "02-2022",
        "total": 50
      }
    ]
  }
]
```

And with applied filters (source: 'Income', 'date': '01-2022')

```json
[
  {
    "source": "Income",
    "data": [
      {
        "date": "01-2022",
        "total": 100
      }
    ]
  }
]
```

We need data returned next way
EXAMPLE!

```json
[
  {
    "source": "Income",
    "data": [
      {
        "date": "01-2022",
        "total": 200
      },
      {
        "date": "02-2022",
        "total": 100
      }
    ]
  },
  {
    "source": "Other",
    "data": [
      {
        "date": "01-2022",
        "total": 1
      },
      {
        "date": "02-2022",
        "total": 50
      }
    ]
  }
]
```

And with applied filters (source: 'Income', 'date': '01-2022')

```json
[
  {
    "source": "Income",
    "data": [
      {
        "date": "01-2022",
        "total": 100
      }
    ]
  }
]
```
